import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import './styles/common.scss';

ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  document.getElementById('root')
);