import { FC } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';

interface IconProps {
    iconName: IconDefinition
}

const Icon: FC<IconProps> = ({ iconName }) => <FontAwesomeIcon icon={iconName}/>

export default Icon;