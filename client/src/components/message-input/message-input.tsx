import { FC, ChangeEvent, FormEvent, KeyboardEvent, useRef } from 'react';
import Icon from '../icon/icon-button';
import { iconName } from '../../constants/icons';
import styles from './styles.module.scss';

interface MessageProps {
    inputValue: string,
    onChange: (e: ChangeEvent<HTMLTextAreaElement>) => void,
    onSubmit: (e: FormEvent<HTMLFormElement>) => void,
}

const MessageInput: FC<MessageProps> = ({ inputValue, onChange, onSubmit }) => {
    const ref = useRef<HTMLFormElement>(null);
    
    const handleKeyDown = (e: KeyboardEvent<HTMLFormElement>) => {
        if (e.key === 'Enter' && !e.shiftKey) {
            onSubmit(e);
        }
    }
    
    return (
        <form
            className={`message-input ${styles.form}`}
            onSubmit={onSubmit}
            ref={ref}
            onKeyDown={handleKeyDown}
        >
            <textarea
                className="message-input-text"
                placeholder="message"
                value={inputValue}
                onChange={onChange}
            />
            <button className="message-input-button">
                <Icon iconName={iconName.send} />
            </button>
        </form>
    );
}


export default MessageInput;