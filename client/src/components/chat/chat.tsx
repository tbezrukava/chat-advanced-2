import { FC, ChangeEvent, FormEvent, useEffect, useState, useMemo } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { dayjs } from '../../helpers/dayjs';
import { remove } from 'lodash';
import Header from '../header/header';
import MessageList from '../message-list/message-list';
import MessageInput from '../message-input/message-input';
import Preloader from '../preloader/preloader';
import ErrorMessage from '../error-message/error-message';
import ApiService from '../../services/api.service';
import { IMessage, HeaderData, HandleOnclickFunc } from '../../helpers/interfaces';
import { chatTitle, currentUser } from '../../constants/api';
import { getLastMessageTime, getParticipantsNum } from '../../helpers/headerHelpers';
import styles from './styles.module.scss';

interface ChatProps {
    url: string
}

const Chat: FC<ChatProps> = ({ url }) =>  {
    const { getData } = new ApiService(url);
    const [messages, setMessages] = useState<IMessage[] | never[]>([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isError, setIsError] = useState(false);
    const [inputValue, setInputValue] = useState('');
    const [editMessageId, setEditMessageId] = useState<string | null>(null)
    const [headerData, setHeaderData] = useState<HeaderData>({
        title: chatTitle,
        usersNum: 0,
        messagesNum: 0,
        lastMessageTime: ''
    });

    useEffect(() => {
        getData()
        .then((res) => {
            setMessages(res);
            setIsLoading(false);
        })
        .catch(() => {
            setIsError(true);
            setIsLoading(false);
        });
    }, []);
    
    useEffect(() => {
        const usersNum = getParticipantsNum(messages);
        const messagesNum = messages.length;
        const lastMessageTime = getLastMessageTime(messages);
        setHeaderData((data) => {
            return { ...data, usersNum, messagesNum, lastMessageTime }
        })
    },[messages.length]);
    
    const handleOnDelete: HandleOnclickFunc = (id) => {
        setMessages((data) => {
            const messages = [...data];
            remove(messages, el => el.id === id);
            return messages;
        });
    
        if (editMessageId === id) {
            setEditMessageId(null);
        }
    };
    
    const handleOnEdit: HandleOnclickFunc = (id) => {
        setEditMessageId(id);
        const editMessageText = messages.find(el => el.id === id)?.text;
        setInputValue(editMessageText!);
    };

    const handleChange = (e: ChangeEvent<HTMLTextAreaElement>) => setInputValue(e.target.value);
    
    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const value = inputValue.trim();
        if (!value) return;
        if (editMessageId) {
            const editMessage = messages.find(el => el.id === editMessageId) as IMessage;
            if (editMessage.text === value) {
                setInputValue('');
                setEditMessageId(null);
                return;
            }
            const editMessageIdx = (messages as IMessage[]).indexOf(editMessage);
            const editedAt = dayjs().format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
            setMessages([
                ...messages.slice(0, editMessageIdx),
                { ...editMessage, text: value, editedAt },
                ...messages.slice(editMessageIdx + 1),
            ]);
            setEditMessageId(null);
        } else {
            const id = uuidv4();
            const message: IMessage = {
                id,
                userId: currentUser.id,
                avatar: currentUser.avatar,
                user: currentUser.name,
                text: value,
                createdAt: dayjs().format('YYYY-MM-DDTHH:mm:ss.SSS[Z]'),
                editedAt: ''
            }
            setMessages(messages => [...messages, message]);
        }
        setInputValue('');
    }

    const header = useMemo(() => <Header {...headerData} />, [headerData]);
    
    const chat = (
        <div className={`chat ${styles.chat}`}>
            {header}
            <MessageList
                messages={messages}
                onDelete={handleOnDelete}
                onEdit={handleOnEdit}
            />
            <MessageInput
                inputValue={inputValue}
                onChange={handleChange}
                onSubmit={handleSubmit}
            />
        </div>
    );
    
    

    return (
        <>
            {isLoading && <Preloader /> || isError && <ErrorMessage /> || chat}
        </>
    );
};

export default Chat;