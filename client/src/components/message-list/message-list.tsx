import { FC, useEffect, useRef } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { uniq } from 'lodash';
import { dayjs } from '../../helpers/dayjs'
import Message from '../message/message';
import OwnMessage from '../own-message/own-message';
import { HandleOnclickFunc, IMessage } from '../../helpers/interfaces';
import { currentUser } from '../../constants/api';
import styles from './styles.module.scss';

interface MessageListProps {
	messages: IMessage[] | never[],
	onDelete: HandleOnclickFunc,
	onEdit: HandleOnclickFunc
}

const MessageList: FC<MessageListProps> = ({ messages, onDelete, onEdit }) => {
	const ref = useRef<HTMLUListElement>(null);
	const dates = uniq(messages.map(el => dayjs.utc(el.createdAt).format('YYYY-MM-DD')));
	const today = dayjs().format('YYYY-MM-DD');
	const yesterday = dayjs().subtract(1, 'day').format('YYYY-MM-DD');
	
	const convertDate = (date: string): string => {
		switch (date) {
			case today:
				return 'Today';
			case yesterday:
				return 'Yesterday';
			default:
				return dayjs(date).format('dddd, MMMM D, YYYY');
		}
	};
	
	const content = dates.map(date => {
		const divider = convertDate(date);
		const dividerKey = uuidv4();
		const messageList =
			messages
			.sort((a, b) => (a.createdAt < b.createdAt) ? -1 : 1)
			.filter(el => dayjs.utc(el.createdAt).format('YYYY-MM-DD') === date)
			.map(el => {
				const isEdited = el.editedAt.length > 0;
				const createTime = dayjs.utc(el.createdAt).format('HH:mm');
				let itemContent = el.userId === currentUser.id ?
					<OwnMessage
						id={el.id}
						text={el.text}
						createTime={createTime}
						isEdited={isEdited}
						onDelete={onDelete}
						onEdit={onEdit}
					/>
					:
					<Message
						user={el.user}
						avatar={el.avatar}
						text={el.text}
						createTime={createTime}
						isEdited={isEdited}
					/>;
				
				return <li key={el.id}>{itemContent}</li>;
			});
		return (
			<li
				key={dividerKey}
				className="messages-divider"
			>
				<h4>{divider}</h4>
				<ul className={styles.messageList}>
					{messageList}
				</ul>
			</li>
		);
	});
	
	// tests crash
	// useEffect(() => ref.current?.scrollTo(0, ref.current?.scrollHeight), [messages.length]);
	
	return(
		<ul
			className={`message-list ${styles.list}`}
			ref={ref}
		>
			{content}
		</ul>
	);
}

export default MessageList;