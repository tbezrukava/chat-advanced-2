import { IMessage } from '../helpers/interfaces';

class ApiService {
    url: string

    constructor(url: string) {
        this.url = url;
    }

    getData = async (): Promise<IMessage[]> => {
        const res = await fetch(this.url);
        if (!res.ok) {
            throw new Error(`Could not fetch ${this.url}, received ${res.status}`);
        }
        return res.json();
    }
}

export default ApiService;