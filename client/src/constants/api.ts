import { v4 as uuidv4 } from 'uuid';

const id = uuidv4();

const currentUser = {
    name: 'John',
    avatar: 'https://padfieldstout.com/wp-content/uploads/2020/01/John_Easter_Padfield_Stout.jpg',
    id
}

const chatTitle = "Work Chat"

export { currentUser, chatTitle };