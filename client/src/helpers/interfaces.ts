interface IMessage {
    id: string,
    userId: string,
    avatar: string,
    user: string,
    text: string,
    createdAt: string,
    editedAt: string
}

interface HeaderData {
    title: string,
    usersNum: number,
    messagesNum: number,
    lastMessageTime: string
}

interface HandleOnclickFunc {
    (id: string): void;
}

export type { IMessage, HeaderData, HandleOnclickFunc };